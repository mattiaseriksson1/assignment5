﻿using System.IO;
using System;
using Assignment5.Repositories;
using Assignment5.Models;
using System.Data.SqlClient;

namespace SQLClient
{
    class program
    {
        //Call the function you would like to test.
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            TestGenre(repository);
        }

        public static void TestCustomersFromCountry(ICustomerRepository repository)
        {
            PrintCustomersCountry(repository.CustomerInCountry());
        }
        public static void TestSpending(ICustomerRepository repository)
        {
            PrintHighestSpenders(repository.HighestSpenders());
        }
        public static void TestGenre(ICustomerRepository repository)
        {
            PrintGenre(repository.CustomerGenre("Wyatt"));
        }
        public static void TestGetByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("wyatt"));
        }
        public static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }
        public static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(6));
        }
        public static void LimitSelect(ICustomerRepository repository)
        {
            PrintCustomers(repository.LimitAllCustomers(100,0));
        }
        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 24,
                Firstname = "Mattias",
                Lastname = "Eriksson",
            };
           if(repository.AddNewCustomer(test))
            {
                Console.WriteLine("Inserted new");
            }
            else
            {
                Console.WriteLine("Didnt work");
            }
        }
        static void TestAddCustomer(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 24,
                Firstname = "Hejsan",
                Lastname = "Bengtsson",
                Country = "Sweden",
                PostalCode = "se-534-12",
                Phone = "1337",
                Email = "HELLOWORLD@csharp.bye",
            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Inserted new");
            }
            else
            {
                Console.WriteLine("Didnt work");
            }
        }
        static void UpdateCustomer(ICustomerRepository repository)
        {
            Customer updatedCustomer = new Customer()
            {   
                CustomerId = 63,
                Firstname = "Lennart",
                Lastname = "Svensson",
                Country = "Skåne",
                Phone = "123456778",
                Email = "Test@mail.com",
                PostalCode = "1337-1337",

            };
            PrintCustomer(repository.UpdateCustomer(updatedCustomer));
            
        }
        static void DeleteCustomer(ICustomerRepository repository)
        {

        }
        /// <summary>
        /// used to go through the list of customers 
        /// </summary>
        /// <param name="customers">needs an object of type customer. </param>
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach(Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        /// <summary>
        ///     Prints customer.
        /// </summary>
        /// <param name="customer">Takes an customer object and prints it to console.</param>
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.Firstname} {customer.Lastname}{customer.Country} {customer.Email}{customer.Phone} {customer.PostalCode}");
        }
        /// <summary>
        ///     Prints the amount of customers per country.
        /// </summary>
        /// <param name="customerCountries">Takes an object of type CustomerCountries.</param>
        private static void PrintCustomersCountry(List<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry Country in customerCountries)
            {
            Console.WriteLine($"---Customers in {Country.Country} = {Country.Count}");
            }
        }
        /// <summary>
        /// Prints list of highest spenders.
        /// </summary>
        /// <param name="spenders">Takes a list of type CustomerSpender.</param>
        private static void PrintHighestSpenders(List<CustomerSpender> spenders)
        {
            Console.WriteLine("Highest Spenders.....");
            foreach (CustomerSpender spender in spenders)
            {
                Console.WriteLine($"name = {spender.FirstName}: Total amount spent = ${spender.Total}");
            }
        }
        /// <summary>
        ///     Prints a specific customer with favorite genre.
        /// </summary>
        /// <param name="genres">Takes a list of type CustomerGenre.</param>
        private static void PrintGenre(List<CustomerGenre> genres)
        {
            foreach (CustomerGenre genre in genres)
            {
                Console.WriteLine($"Name = {genre.Name} Popularity = {genre.Popularity}: Favorite genre = {genre.Genre}");
            }
        }

    }
}
