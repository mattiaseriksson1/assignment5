﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment5.Models
{
    public class CustomerGenre
    {
        public string Name { get; set; }
        public int Popularity { get; set; }
        public string Genre { get; set; }
    }
}
