﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment5.Models;
using SQLClient;

namespace Assignment5.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Get all the Customers
        /// </summary>
        /// <returns>A list of all the customers</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country,PostalCode, Phone,Email FROM Customer WHERE Phone IS NOT NULL" +
                " AND PostalCode IS NOT NULL";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return custList;
        }
        /// <summary>
        /// Get the Customer by name
        /// </summary>
        /// <param name="name">Name that you would like to get</param>
        /// <returns>Returns a Object of type Customer</returns>
        public Customer GetCustomerByName(string name)
        {

            string sql = "SELECT CustomerId, FirstName, LastName, Country,PostalCode, Phone,Email FROM Customer WHERE FirstName LIKE @FirstName";
            Customer customer = new Customer();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                customer.CustomerId =  reader.GetInt32(0);
                                customer.Firstname = reader.GetString(1);
                                customer.Lastname = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4) ;
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
             
                Console.WriteLine($"Someting went wrong - {ex.Message}");
            }
            catch (SqlNullValueException ex) {
                Console.WriteLine($"WASSSUSSUSUUSUSp");
                Console.WriteLine($"Someting went wrong - {ex.Message}");
            }

            return customer;
        }
        /// <summary>
        /// Get specific Customer based on ID
        /// </summary>
        /// <param name="id">Id of customer you would like to get </param>
        /// <returns>Returns an object of type Customer</returns>
        public Customer GetCustomer(int id)
        {
            string sql = "SELECT CustomerId, FirstName, LastName, Country,PostalCode, Phone,Email FROM Customer" + " WHERE CustomerId = @CustomerId AND Phone IS NOT NULL AND PostalCode IS NOT NULL";
            Customer temp = new Customer();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                };
            }

            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return temp;
        }
        /// <summary>
        /// Get customers with a limit
        /// </summary>
        /// <param name="limit">The number of customers you would like to show.</param>
        /// <param name="offset">The number of skipped rows.</param>
        /// <returns>A list of Customers.</returns>
        public List<Customer> LimitAllCustomers(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country,PostalCode, Phone,Email FROM Customer WHERE PostalCode IS NOT NULL AND Phone IS NOT NULL ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY ";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                           
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4) ;
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            catch (SqlNullValueException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");

            }
            return custList;
        }
        /// <summary>
        /// Insert new customer into the database.
        /// </summary>
        /// <param name="customer">Takes an object of type Customer.</param>
        /// <returns>a Success message if able to add to database. otherwise error message.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email)" +
                " VALUES (@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";

            try
            {
                using(SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.Firstname);
                        cmd.Parameters.AddWithValue("@LastName", customer.Lastname);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return success;
        }
        /// <summary>
        /// Update customer in database.
        /// </summary>
        /// <param name="customer">Takes an object of type Customer.</param>
        /// <returns>Returns the Customer that gets updated.</returns>
        public Customer UpdateCustomer(Customer customer)
        {

            string sql = "UPDATE Customer SET FirstName = @CustomerNewName, Lastname = @Lastname, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId  "; 
                
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerNewName", customer.Firstname);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@Lastname", customer.Lastname);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.Firstname = reader.GetString(1);
                                temp.Lastname = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = reader.GetString(4);
                                temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);

                            }
                        }
                    }
                };
            }

            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return customer;
        }
        /// <summary>
        ///     Summarizes the amount of customers in each country.
        /// </summary>
        /// <returns>The list of all customers in specific countries.</returns>
        public List<CustomerCountry> CustomerInCountry()
        {
            List<CustomerCountry> CountryList = new List<CustomerCountry>();
            string sql = "SELECT COUNT(Country) AS NumberOfCustomers, Country  FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry tempTwo = new CustomerCountry();
                                tempTwo.Country = reader.GetString(1);
                                tempTwo.Count = reader.GetInt32(0);
                                CountryList.Add(tempTwo);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return CountryList;
        }
        /// <summary>
        ///     Get a list of the highest spenders.
        /// </summary>
        /// <returns>the list of the spenders.</returns>
        public List<CustomerSpender> HighestSpenders()
        {
            List<CustomerSpender> Spenders = new List<CustomerSpender>();
            string sql = "SELECT Customer.FirstName, SUM(Total) as Total FROM Customer " +
                "INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId GROUP BY FirstName ORDER BY Total DESC; ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender tempTwo = new CustomerSpender();
                                tempTwo.FirstName = reader.GetString(0);
                                tempTwo.Total = reader.GetDecimal(1);
                                Spenders.Add(tempTwo);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return Spenders;
        }
        /// <summary>
        ///     Get the most popular genre for a specific customer.
        /// </summary>
        /// <param name="name">The name of the specific customer.</param>
        /// <returns>A list with the customers favourite genre.</returns>
        public List<CustomerGenre> CustomerGenre(string name)
        {
            
            List<CustomerGenre> genre = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 Genre.Name, COUNT(Genre.Name) AS popularity" +
                " FROM ((((Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId )" +
                " INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId )" +
                " INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId )" +
                " INNER JOIN Genre ON Track.GenreId = Genre.GenreId )" +
                " WHERE FirstName = @FirstName GROUP BY Genre.Name Order By popularity DESC ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre tempTwo = new CustomerGenre();
                                tempTwo.Popularity = reader.GetInt32(1);
                                tempTwo.Genre = reader.GetString(0);
                                tempTwo.Name = name; 
                                genre.Add(tempTwo);
                            }
                        }
                    }
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error - {ex.Message}");
            }
            return genre;
        }
    }
 }

