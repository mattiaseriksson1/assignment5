﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment5.Models;

namespace Assignment5.Repositories
{
    public interface ICustomerRepository
    {
        //Implemented Repository pattern.
        public Customer GetCustomer(int id);
        public List<Customer> GetAllCustomers();
        public List<Customer> LimitAllCustomers(int limit, int offset);
        public Customer GetCustomerByName(string name);
        public bool AddNewCustomer(Customer customer);
        public Customer UpdateCustomer(Customer customer);
        public List<CustomerCountry> CustomerInCountry();
        public List<CustomerSpender> HighestSpenders();
        public List<CustomerGenre> CustomerGenre(string name);
    }
}
