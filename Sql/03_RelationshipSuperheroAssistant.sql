USE SuperHeroesDb

ALTER TABLE Assistant 
ADD HeroID Int CONSTRAINT FK_SuperHeroAssistant FOREIGN KEY REFERENCES Superhero(HeroID)

CREATE TABLE LinkingHeroAssistant(
HeroID int  FOREIGN KEY REFERENCES Superhero(HeroID) , 
AssistantID  int  FOREIGN KEY REFERENCES Assistant(AssistantID),
  PRIMARY KEY (HeroID, AssistantID) 
)
