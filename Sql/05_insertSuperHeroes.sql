use SuperHeroesDb;

INSERT INTO Superhero( HeroName, Alias, Origin) VALUES ( 'Batman', 'Bruce Wayne', 'Dead Parents' );
INSERT INTO Superhero( HeroName, Alias, Origin) VALUES ( 'Superman', 'Clark Kent', 'Alien' );
INSERT INTO Superhero( HeroName, Alias, Origin) VALUES ( 'Catwoman', 'Selina Kyle', 'Cats' );
