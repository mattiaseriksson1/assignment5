USE SuperHeroesDb

CREATE TABLE Superhero (
HeroID int NOT NULL PRIMARY KEY IDENTITY(1,1),
HeroName nvarchar(50) null,
Alias nvarchar(50) null, 
Origin nvarchar(50) null
)
CREATE TABLE Assistant (
AssistantID int NOT NULL PRIMARY KEY IDENTITY(1,1),
AssistantName nvarchar(50) null

)
CREATE TABLE HeroPower (
PowerID int NOT NULL PRIMARY KEY IDENTITY(1,1),
PowerName nvarchar(50) null,
Description nvarchar(50) null

)