USE SuperHeroesDb

ALTER TABLE HeroPower
ADD HeroID Int CONSTRAINT FK_SuperheroPower FOREIGN KEY REFERENCES Superhero(HeroID);

CREATE TABLE LinkingHeroPower(
HeroID int FOREIGN KEY REFERENCES Superhero(HeroID) , 
PowerID int FOREIGN KEY REFERENCES HeroPower(PowerID),
PRIMARY KEY (HeroID, PowerID)
)

