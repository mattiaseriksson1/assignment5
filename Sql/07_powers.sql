use SuperHeroesDb;

INSERT INTO HeroPower(PowerName, Description, HeroID) VALUES ('Hard-punch', 'Punching really hard',2);
INSERT INTO HeroPower(PowerName, Description, HeroID) VALUES ('Superspeed', 'Running fast', 3);
INSERT INTO HeroPower(PowerName, Description, HeroID) VALUES ('High-jump', 'Jumping high',1);
INSERT INTO HeroPower(PowerName, Description, HeroID) VALUES ('Invisibility', 'Turning invisible', 1);