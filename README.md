# Assignment5
Appendix A: in SQL-folder
Featuring SQL queries

Appendix B: in rest of the solution.
Featuring C# console application with SQL Queries.

## Install/Usage
Application requires a SQL-server. 

In the "ConnectionStringHelper" file, change the connectionStringBuilder.Datasource to your own instance of SQL server.



## Contributers 
[Mattias Eriksson ](@MattiasLeifEriksson)
[Daniel Bengtsson ](@DanielBengtsson) 
